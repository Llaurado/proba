import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Jdbc {
    private static Connection myconn;
    private static ArrayList<Client> llista;
    private static ArrayList<Comanda> comanda;
    private static ArrayList<Object> total;

    static {
        try {
            myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/basededatos", "usuario", "jupiter");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static int menu() {
        System.out.println("Operacions:\n1.Borrar CLient\n2.Update client\n3.Mostrar client\n4.Alta nou client\n5.Nova comanda\n6.Mostrar comanda\n7.Generacio resum fact\n0.Sortir");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();

    }

    public static void borrarClient() throws SQLException {

        Scanner sc = new Scanner(System.in);


        Statement mystmn = myconn.createStatement();

        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        String client = sc.next();
        String borrar ="delete from comandes where dni_client="+client;
        String borrar2 ="delete from client where nif="+client;
        mystmn.execute(borrar);
        mystmn.execute(borrar2);
        System.out.println("Client borrat juntamens amb les seves comandes");

    }

    public static void updateclient() throws SQLException {



        Scanner sc = new Scanner(System.in);


        Statement mystmn = myconn.createStatement();

        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        String client = sc.next();


        System.out.println("2.nom");
        String nom=sc.next();
        System.out.println("3.Es vip?(True or false)");
        Boolean vip = sc.nextBoolean();





        String update="update client set nom="+nom+",premium="+vip+"where nif="+client;
        mystmn.execute(update);




    }


    public static void mostrarclient() throws SQLException, ParseException {
        Scanner sc=new Scanner(System.in);
        System.out.println("text a introduir per cercar nom del client");;
        String nom=sc.next();
        Statement st=myconn.createStatement();
       String select="select * from client where nif LIKE %"+nom+"%";
       ResultSet rs=st.executeQuery(select);
       while (rs.next()){
           System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getBoolean(3));
       }



    }

    public static void altaClient() throws SQLException {

        String sentenciaSQL = "insert into client (nif,nom,premium)  values (?,?,?)";
        PreparedStatement sentenciaPreparada = myconn.prepareStatement(sentenciaSQL);



        Scanner sc = new Scanner(System.in);
        Client c1;

        int dni;
        String nom;
        boolean vip;
        Comanda com = null;
        System.out.println("Alta Client-----\n1.dni(sense lletra)");
        dni = sc.nextInt();
        System.out.println("2.nom");

        nom = sc.next();
        System.out.println("3.Es vip?(True or false)");
        vip = sc.nextBoolean();



        for (int i = 0; i < llista.size(); i++) {
            System.out.println(llista.get(i));
        }
        sentenciaPreparada.setInt(1, dni);
        sentenciaPreparada.setString(2, nom);
        sentenciaPreparada.setBoolean(3, vip);
        sentenciaPreparada.executeUpdate();
        System.out.println("Client inserit correctament.");


    }

    public static void altacomanda() throws SQLException, ParseException {
        String sentenciaSQL1 = "insert into comandes (num_com,preu_total,data,dni_client) values (?,?,?,?)";
        PreparedStatement sentenciaPreparada1 = myconn.prepareStatement(sentenciaSQL1);

        Scanner sc = new Scanner(System.in);


        int num_com;
        double preu;
        String data;

        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        int client = sc.nextInt();


        System.out.println("Alta Client-----\n1.num_com");
        num_com = sc.nextInt();

        System.out.println("2.preu");
        preu = sc.nextDouble();

        System.out.println("3.data");
        data = sc.next();
        Date a = new SimpleDateFormat("dd/MM/yyy").parse(data);
        sentenciaPreparada1.setInt(1, num_com);
        sentenciaPreparada1.setDouble(2, preu);
        sentenciaPreparada1.setDate(3, (java.sql.Date) a);
        sentenciaPreparada1.setInt(4, client);
        sentenciaPreparada1.executeUpdate();
        System.out.println("Comanda inserit correctament.");




    }

    public static void mostrarcom() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Escriu el dni del client");
        for (int i = 0; i < llista.size(); i++) {
            System.out.println("Dni:" + llista.get(i).getDni() + " Nom:" + llista.get(i).getName());
        }
        int client = sc.nextInt();

        for (int i = 0; i < llista.size(); i++) {
            if (client == (llista.get(i).getC1().getDni())) {
                System.out.println(comanda.get(i));
            }

        }
    }
    public static  void  generres_fact(int mes,int year,int dni) throws SQLException {
        Calendar cal= Calendar.getInstance();
        int añoactual=cal.get(Calendar.YEAR);
        int mesactual=cal.get(Calendar.MONTH);
        Statement st= null;

            st = myconn.createStatement();

        if(mes>0&&mes<13&&year<=añoactual&&mes<=mesactual){
            String call="call procedure proced_emmagatzemat("+mes+","+year+","+dni+")";

            st.execute(call);
            myconn.setAutoCommit(false);
            System.out.println("dates correctes");
        }else{
            System.out.println("mes o any incorrectes");
        }



    }

    public static void main(String[] args) throws SQLException, ParseException {
        Scanner sc=new Scanner(System.in);
        int opcio = menu();
        while (opcio != 0) {
            switch (opcio) {
                case 1:
                    borrarClient();
                    break;
                case 2:
                    updateclient();

                    break;
                case 3:
                    mostrarclient();

                    break;
                case 4:
                    altaClient();
                    break;
                case 5:
                    altacomanda();
                    break;
                case 6:
                    mostrarcom();
                    break;
                case 7:
                    System.out.println("Introdueix un mes valid:  ");
                    int mes=sc.nextInt();
                    System.out.println("Introdueix un any valid:  ");
                    int any=sc.nextInt();
                    System.out.println("Introdueix un dni:  ");
                    int dni=sc.nextInt();

                    generres_fact(mes,any,dni);
                    break;
            }

            opcio = menu();
        }
    }
}
/*              "DELIMITER $$ " +
                "Create procedure proced_emmagatzemat(mes_func int, any_func int , dni int) " +
                "begin" +
                "set @data_ini=CONCAT(any_func,'-',mes_func,'-',01);" +
                "set @data_fi=CONCAT(any_func,'-',mes_func,'-',31);" +
                "set @dni_res=(select dni_client from comandes where dni_client=dni and data between @data_ini and @data_fi group by dni_client having count(*)>=1 ); " +
                "set @cant=(select sum(preu_total) from comandes where dni_client=dni and data between @data_ini and @data_fi group by dni_client having count(*)>=1  LIMIT 1  ); " +
                " insert into res_fact(mes,year,dni_client,quantitat) values(mes_func,any_func,@dni_res,@cant)          ;" +
                "set @correct=\'se ha creado bien\';" +
                "select @correct;" +
                "END$$ " +
                "DELIMITER ;"*/







