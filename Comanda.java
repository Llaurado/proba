import java.sql.Date;
import java.text.DateFormat;

public class Comanda {

    private int num_com;
    private double preu;
    private String date;
    private int dni;

    public Comanda(int num_com, double preu, String date, int dni) {
        this.num_com = num_com;
        this.preu = preu;
        this.date = date;
        this.dni = dni;
    }

    public int getNum_com() {
        return num_com;
    }

    public void setNum_com(int num_com) {
        this.num_com = num_com;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "num_com=" + num_com +
                ", preu=" + preu +
                ", date='" + date + '\'' +
                ", dni=" + dni +
                '}';
    }
}
